#!/bin/bash

awk '
BEGIN { FS=", "; }
{ print "define(NAME_OF_USER,"$1")dnl" > "tempdef";
  print "define(EMAIL_OF_USER,"$2")dnl" >> "tempdef";
  print "define(PAN_OF_USER,"$3")dnl" >> "tempdef";
  print "define(DONATION_OF_USER,"$4")dnl" >> "tempdef";
	
  system("m4 tempdef mail_template.m4 > temp_mail");
  
  print "HELO smtp.cse.iitk.ac.in" > "final_mail"
  print "MAIL FROM: <rahi@cse.iitk.ac.in>" >> "final_mail"
  print "RCPT TO: <rahi@cse.iitk.ac.in>" >> "final_mail"

  print "DATA" >> "final_mail"
  print "From: <rahi@cse.iitk.ac.in>" >> "final_mail"
  printf "To: <%s>\n",$2 >> "final_mail"
  system("cat temp_mail >> final_mail");
  system("sleep 1");
  system("nc smtp.cse.iitk.ac.in 25 < final_mail");
  close("tempdef");
  close("temp_mail");
  close("final_mail");

}
END{}' details.csv


