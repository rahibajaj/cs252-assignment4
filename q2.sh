#!/bin/bash

awk '
BEGIN {	FS=", ";}
{
	print "define(NAME_OF_USER," $1 ")dnl" > "tempdef";
	print "define(EMAIL_OF_USER," $2 ")dnl" >> "tempdef";
	print "define(PAN_OF_USER," $3 ")dnl" >> "tempdef";
	print "define(DONATION_OF_USER," $4 ")dnl" >> "tempdef";
	system("m4 tempdef mail_template.m4 | msmtp -t " $2);
	close("def");
}' details.csv
